import numpy as np
from rl.core import Processor
import xlrd as xd
import re
import time
from envConnect import *

def completedProductFromJobList(jobNumber):
    workbook = xd.open_workbook('C:\\Users\\IFT\\Desktop\\OS.xlsx')
    worksheet = workbook.sheet_by_name('joblist')
    productNumber =  (worksheet.cell(jobNumber - 1,0).__str__()).split(':')
    temp = re.findall(r'\d+', productNumber[1])
    for product in temp:
        productNumber = int(product)
    return productNumber

def getJobCompleted():
    time.sleep(8)
    workbook = xd.open_workbook('C:\\Users\\IFT\\Desktop\\OS.xlsx')
    worksheet = workbook.sheet_by_name('count')
    try:
        jobValue = (worksheet.cell(1,0).__str__()).split(':')
    except IndexError:
        jobValue = 'null'

    if jobValue == 'null':
        jobNum = 0
    else:
        jobNum = int(float(jobValue[1]))
        jobNum = completedProductFromJobList(jobNumber=jobNum)
        print('Completed Job',jobNum)


    return jobNum

class OneHotNNInputProcessor(Processor):
    '''
    OneHotNNInputProcessor is a pre-processor for neural network input, it pre-processes and return possible tensor
    In particular it encodes each of the 60 products with a one-hot encoding method, and represents them with 1-D matrix made up of
    0s and 1s equal to num_one_hot_matrices.'''

    def process_observation(self, observation):
        '''jobNum = getJobCompleted()
        if jobNum > 0:
            observation = self.observation
        else:
            for i in range (1,60):
                self.observation[i,0] = 0
                self.observation[i,1] = 0
                self.observation[i,2] = 1
            observation = self.observation'''
        return self.observation



    def process_state_batch(self, batch):

        batch = np.reshape(batch, (12,12,17))

        return batch


