import random
import pickle

from keras.layers import Dense, Flatten
from keras.models import Sequential,load_model
#from keras.models import Model
from keras.optimizers import Adam
from rl.agents.dqn import DQNAgent
from rl.callbacks import ModelIntervalCheckpoint
from rl.memory import SequentialMemory
from rl.policy import EpsGreedyQPolicy
from rl.policy import BoltzmannQPolicy
from rl.policy import LinearAnnealedPolicy

from callbackSimulation import TrainEpisodeLogger, TestLogger
from envConnect import *

# number of observations from the environment to provide to the neural network/DQN agent
#window_length = 12
#input_shape = (12,2)

# Create the environment for DQN Agent
env = SimulationEnv()

window_length = 1
# Set a specific seed for the pseudo-random number generators to obtain reproducible results:
random_seed = 1
random.seed(random_seed)
np.random.seed(random_seed)
env.seed(random_seed)

# Initialize the environment


def dnn_model():

    #Preprocess the data before feeding to the network
    #processor = OneHotNNInputProcessor()
    # 12 input nodes
    input_shape = (window_length*1,)+ (100,7)  # represent to the input shape for the neural network

    model = Sequential()
    model.add(Flatten(input_shape=input_shape))  # (1,12,1)
    model.add(Dense(units=512, activation='relu'))    # layer: (64,128)  # activation: tanh, sigmoid, relu.
    model.add(Dense(units=512, activation='relu'))
    #model.add(Dense(units=512, activation='relu'))

    #model.add(Dense(units=128, activation='relu'))
    model.add(Dense(units=100, activation='linear'))     #in dqn file softmax
    print(model.summary())
    #model.to_json('modelToJson.h5')
    '''model = Sequential()
    model.add(Conv2D(32, kernel_size=(1,3), activation='relu', input_shape=input_shape)) ## row,col
    model.add(MaxPooling2D(pool_size=(1,2)))
    model.add(Conv2D(32, kernel_size=(1,3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(1,2)))
    model.add(Flatten())
    model.add(Dense(units=32, activation='relu'))
    model.add(Dense(20, activation='softmax'))
    print(model.summary())'''

    return model

def q_learn(model,mode, memoryFile,pretrain,modelFile):

    #  NN training via fitting of Q-learning equation
    ## Training Memory: Seq Memory used to store the agents state observations for training
    memory = SequentialMemory(limit=500000, window_length=1)

    if pretrain:
        # save the memory in the memoryfile
        (memory, memory.actions,memory.rewards,memory.terminals,memory.observations) = pickle.load(open(memoryFile, "rb"))
    else:
        mem = (memory, memory.actions, memory.rewards, memory.terminals, memory.observations)
        pickle.dump(mem, open(memoryFile, "wb"), protocol=-1)



    #memory = PrioritizedMemory(limit=9000000, alpha=.6, start_beta=.4, end_beta=1., window_length=window_length)
    #TRAIN_POLICY = LinearAnnealedPolicy(EpsGreedyQPolicy(), attr='eps', value_max=1., value_min=.1, value_test=0.05,nb_steps=int(2e4))
    #TRAIN_POLICY = BoltzmannQPolicy(tau=0.01)
    #TRAIN_POLICY = RandomActionQPolicy()

    TRAIN_POLICY = LinearAnnealedPolicy(EpsGreedyQPolicy(), attr='eps', value_max=0.9, value_min=0.0001, value_test=0.01, nb_steps=5000)
    TEST_POLICY = EpsGreedyQPolicy(eps=.0001)
    #TRAIN_POLICY = EpsGreedyQPolicy(eps=.01)


    dqn = DQNAgent(model=model, nb_actions=100, test_policy=TEST_POLICY, policy=TRAIN_POLICY, processor= None, memory=memory,nb_steps_warmup=500,
                   gamma=.99, target_model_update=0.001, train_interval=1)#, batch_size = 256)#, 128, 64, 32
    # Training Method & Metric:
    #    We use the Adam learning method with MSE (mean squared error) metric.
    #dqn.compile(Adam(lr=0.0001), metrics=['mae'])
    #lr = 0.0001
   #lr = .00025
    lr = .00005 #best for 5000wp



    '''if type(memory) == PrioritizedMemory:
        lr /= 4'''
    dqn.compile(Adam(lr=lr), metrics=['mae'])

    episode = 'icos_one100,512-512, sm500000,500wu, 5000ep, av0-100, r100'


    # TRAIN
    if mode == 'TRAIN':

        if pretrain:
            dqn.load_weights(modelFile)


        # always creates a new file for each experiment. Incase of pretrained model it will load the weights for the new model and create a file for it.

        weights_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode ,'shuttle','dnn')
        csv_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode,'shuttle','dnn')
        checkpoint_weights_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode,'shuttle','dnn')

        # Callbacks: Stored with a list of Callbacks() functions
        # ModelIntervalCheckpoint is a callback used to save the NN weights every few steps.
        _callbacks = [ModelIntervalCheckpoint(checkpoint_weights_filepath, interval=25000)]
        # TrainEpisodeLogger2048 is a callback used to plot some charts, updated in real-time, that show the NN training results and to save the results in a CSV file.
        _callbacks += [TrainEpisodeLogger(csv_filepath)]


        # fit() starts the actual training of the model.
        dqn.fit(env, callbacks=_callbacks, nb_steps=5000, visualize=False, verbose=0, nb_max_episode_steps=100)


        # Save the final weights after the training is completed
        time.sleep(4)
        if pretrain:
            dqn.save_weights(weights_filepath, overwrite=True)
        else:
            dqn.save_weights(weights_filepath, overwrite=True)
        env.reset()

   # TEST
    elif mode == 'TEST':
        #time.sleep(4)
        weights_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode,'shuttle','dnn')
        dqn.load_weights(weights_filepath)
        _callbacks = [TestLogger()]
        dqn.test(env, nb_episodes=10,visualize=False, verbose=0, callbacks =_callbacks, nb_max_episode_steps=100)

def main(mode,pretrain):
    #plantSim = initializeEnv()

    memoryFile = 'icos_one100,512-512, sm500000,500wu, 5000ep, av0-100, r200.pickle'

    if pretrain:
        modelFile = 'Episode_icos_one100,512-512, sm500000,500wu, 5000ep, av0-100, r200_dqn_shuttle_dnn.h5' # put here pretrained model file
    else:
        modelFile = None

    model = dnn_model()
    q_learn(model=model,mode=mode,memoryFile=memoryFile, pretrain=pretrain, modelFile=modelFile)

if __name__ == '__main__':
    main(mode='TRAIN',pretrain=False) # Pretrain = True if model is already trained,  = False if training new model







