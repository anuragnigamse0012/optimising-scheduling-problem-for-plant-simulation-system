import random
import pickle

from keras.layers import Dense, Flatten
from keras.models import Sequential
from keras.optimizers import Adam

from rl.agents.dqn import DQNAgent
from rl.callbacks import ModelIntervalCheckpoint
from rl.memory import SequentialMemory
from rl.policy import EpsGreedyQPolicy,LinearAnnealedPolicy

from callbackSimulation import TrainEpisodeLogger, TestLogger
from envConnect import *

class ShuttleSimulation:

    def __init__(self):
        self.env = SimulationEnv()
        self.window_length = 1
        # Set a specific seed for the pseudo-random number generators to obtain reproducible results:
        random_seed = 1
        random.seed(random_seed)
        np.random.seed(random_seed)
        self.env.seed(random_seed)

    def dnn_model(self):
        ''' Preprocess the data before feeding to the network
        processor = OneHotNNInputProcessor()
        12 input nodes'''

        input_shape = (self.window_length*1,)+ (100,7)  # represent to the input shape for the neural network
        model = Sequential()
        model.add(Flatten(input_shape=input_shape))  # (1,12,1)
        model.add(Dense(units=512, activation='relu'))    # layer: (64,128)  # activation: tanh, sigmoid, relu.
        model.add(Dense(units=512, activation='relu'))
        #model.add(Dense(units=512, activation='relu'))

        #model.add(Dense(units=128, activation='relu'))
        model.add(Dense(units=100, activation='linear'))     #in dqn file softmax
        print(model.summary())

        return model

    def q_learn(self,model,mode, memoryFile,pretrain,modelFile):
        """

        :param model:
        :param mode:
        :param memoryFile:
        :param pretrain:
        :param modelFile:
        :return:
        """
        '''NN training via fitting of Q-learning equation
            Training Memory: Seq Memory used to store the agents state observations for training'''
        memory = SequentialMemory(limit=500000, window_length=1)
        if pretrain:
            # save the memory in the memoryfile
            (memory, memory.actions,memory.rewards,memory.terminals,memory.observations) = pickle.load(open(memoryFile, "rb"))
        else:
            mem = (memory, memory.actions, memory.rewards, memory.terminals, memory.observations)
            pickle.dump(mem, open(memoryFile, "wb"), protocol=-1)
            TRAIN_POLICY = LinearAnnealedPolicy(EpsGreedyQPolicy(), attr='eps', value_max=0.9, value_min=0.0001, value_test=0.01, nb_steps=10000)
            TEST_POLICY = EpsGreedyQPolicy(eps=.00001)

            dqn = DQNAgent(model=model, nb_actions=100, test_policy=TEST_POLICY, policy=TRAIN_POLICY, processor= None, memory=memory,nb_steps_warmup=500,
                   gamma=.99, target_model_update=0.001, train_interval=1)#, batch_size = 256)#, 128, 64, 32
            # Training Method & Metric:
            #  We use the Adam learning method with MSE (mean squared error) metric.

            lr = .00005
            dqn.compile(Adam(lr=lr), metrics=['mae'])

            episode = 'cos_one100,512-512, sm500000,500wu, 10000ep, av0-100, r 100'
            # TRAIN
            if mode == 'TRAIN':
                if pretrain:
                    dqn.load_weights(modelFile)
                # always creates a new file for each experiment. Incase of pretrained model it will load the weights for the new model and create a file for it.

                weights_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode ,'shuttle','dnn')
                csv_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode,'shuttle','dnn')
                checkpoint_weights_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode,'shuttle','dnn')

                # Callbacks: Stored with a list of Callbacks() functions
                # ModelIntervalCheckpoint is a callback used to save the NN weights every few steps.
                _callbacks = [ModelIntervalCheckpoint(checkpoint_weights_filepath, interval=25000)]
                # TrainEpisodeLogger2048 is a callback used to plot some charts, updated in real-time, that show the NN training results and to save the results in a CSV file.
                _callbacks += [TrainEpisodeLogger(csv_filepath)]
                # fit() starts the actual training of the model.
                dqn.fit(self.env, callbacks=_callbacks, nb_steps=10000, visualize=False, verbose=0, nb_max_episode_steps=100)
                # Save the final weights after the training is completed
                time.sleep(4)
                if pretrain:
                    dqn.save_weights(weights_filepath, overwrite=True)
                else:
                    dqn.save_weights(weights_filepath, overwrite=True)
                self.env.reset()

            # TEST
            elif mode == 'TEST':
                #time.sleep(4)
                weights_filepath = 'Episode_{}_dqn_{}_{}.h5'.format(episode,'shuttle','dnn')
                dqn.load_weights(weights_filepath)
                _callbacks = [TestLogger()]
                dqn.test(self.env, nb_episodes=10,visualize=False, verbose=0, callbacks =_callbacks, nb_max_episode_steps=80)

    def main(self,mode,pretrain):
        memoryFile = 'cos_one100,512-512, sm500000,500wu, 10000ep, av0-100, r 100.pickle'

        if pretrain:
            modelFile = 'Episode_cos_one100,512-512, sm500000,500wu, 10000ep, av0-100, r 100_dqn_shuttle_dnn.h5' # put here pretrained model file
        else:
            modelFile = None

        model = self.dnn_model()
        self.q_learn(model=model,mode=mode,memoryFile=memoryFile, pretrain=pretrain, modelFile=modelFile)

if __name__ == '__main__':
    obj = ShuttleSimulation()
    obj.main(mode='TEST',pretrain=False) # Pretrain = True if model is already trained,  = False if training new model







