import re
import xlrd as xd
import time
import numpy as np
from comtypes.client import CreateObject
import os

import gym
from gym import *
from gym.utils import seeding
from gym import spaces

# initialising an empty object
global environment

plantSim = "global"


class SimulationEnv(gym.Env):

    def __init__(self):
        print('initialise the environment')
        self.action_space = np.arange(1,101) # action_space = number of the items
        self.model = "dnn"
        self.observation = spaces.Box(low=0,high=100,shape=(100,7), dtype=np.float16) #
        self.filelocation = 'C:\\Users\\IFT\\Desktop\\job_list.xlsx'

        self.plantSim = CreateObject("Tecnomatix.PlantSimulation.RemoteControl.14.0")
        self.plantSim.loadModel("C:\\Users\\IFT\\Desktop\\100+xav+complete_connection - Test_2S.spp")
        self.plantSim.setVisible(True)
        # Psuedo random number generator, uses the seed value as a base to generate a random number.
        self.seed()
        self.value = 0

    def get_completed_actions(self,action_v1):
        """
        Retrieve the action completed by the other vehicle and append them with the current action into list.
        Call the get_observation method to get new observation states.
        :param action_v1: Current action assigned to the model vehicle
        :return:
        """

        rows = self.plantSim.getValue(".Modelle.Netzwerk.jobs_otherShuttles.ydim")
        for i in range(rows):
            self.plantSim.setValue(".Modelle.Netzwerk.num_jo",i+1)
            value = self.plantSim.getValue(".Modelle.Netzwerk.jobs_otherShuttles[1,.Modelle.Netzwerk.num_jo]")
            action_v2 = int(re.findall(r'\d+', value)[0])
            if action_v2 not in self.items:
                self.items.append(int(action_v2))
                self.set_observation(action_v2)
            else:
                continue
        if action_v1 not in self.items:
            self.items.append(int(action_v1))
            self.set_observation(action_v1)


    def set_observation(self, action): #15
        """
        Modifies the observation state for the action already completed
        :param states: set of completed action
        :return:
        """
        self.num_hot_encoding[action-1,0] = 200
        #print(self.observation)

###################to implement location and destination of the other vehicle in 5D-OS#############
        dest_aisleOV = self.plantSim.getValue(".Modelle.Netzwerk.destaisleS2") #2
        dest_posOV = self.plantSim.getValue(".Modelle.Netzwerk.destpositionS2") #3
        aisle_OV = self.plantSim.getValue(".Modelle.Netzwerk.currentaisleS2") #3
        pos_OV = self.plantSim.getValue(".Modelle.Netzwerk.currentpositionS2") #4

        #print('Simulation Values :',dest_aisleOV,dest_posOV,aisle_OV,pos_OV)

        for i in range(100):
            if (i+1) not in self.items:
                self.num_hot_encoding[i,1] = aisle_OV
                self.num_hot_encoding[i,2] = pos_OV
                self.num_hot_encoding[i,3] = dest_aisleOV
                self.num_hot_encoding[i,4] = dest_posOV
            elif (i+1) in self.items:
                self.num_hot_encoding[i, 1] = 0.0
                self.num_hot_encoding[i, 2] = 0.0
                self.num_hot_encoding[i, 3] = 0.0
                self.num_hot_encoding[i, 4] = 0.0
                self.num_hot_encoding[i, 5] = 0.0
                self.num_hot_encoding[i, 6] = 0.0
##################################


        #print(self.observation)


    def step(self, action):
        """Run one timestep of the environment's dynamics.
        Accepts an action and returns a tuple (observation, reward, done, info).

        # Arguments
            action (object): An action provided by the environment.

        # Returns
            observation (object): Agent's observation of the current environment.
            reward (float) : Amount of reward returned after previous action.
            done (boolean): Whether the episode has ended, in which case further step() calls will return undefined results.
            info (dict): Contains auxiliary diagnostic information (helpful for debugging, and sometimes learning).
        """

        if action not in self.items:
            reward = float(self.move(action))
            self.get_completed_actions(action_v1=action)
            new_observation = self.num_hot_encoding
            info = {'action':action, 'step_value': int(self.value) + int(len(self.items)), 'action_list':self.items}
            return new_observation, reward, info

        elif action in self.items:
            #print('Action Out:',action)
            reward = -100
            new_observation = self.num_hot_encoding
            self.value += 1
            info = {'action':action, 'step_value':int(self.value)+int(len(self.items)), 'action_list':self.items}

            return new_observation, reward, info

    def reset(self):
        """
        Reset the simulation model and initialise it again.
        :return:
        """
        simfin = self.plantSim.getValue(".Modelle.Netzwerk.simfin")
        if simfin:
            self.items = []
            self.value = 0
            self.count = self.newCount = 0
            self.plantSim.resetSimulation(".Modelle.Netzwerk.Ereignisverwalter")
            time.sleep(2)
            self.plantSim.executeSimTalk(".Modelle.Netzwerk.initialize")
            time.sleep(2)
            self.plantSim.setVisible(True)

            self.num_hot_encoding = np.zeros(shape=(100, 7), dtype=np.float16)

            self.num_hot_encoding[0] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,1]"), 0, 0, 0, 0, 50, 4]
            self.num_hot_encoding[1] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,2]"), 0, 0, 0, 0, 50, 8]
            self.num_hot_encoding[2] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,3]"), 0, 0, 0, 0, 50, 12]
            self.num_hot_encoding[3] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,4]"), 0, 0, 0, 0, 50, 16]
            self.num_hot_encoding[4] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,5]"), 0, 0, 0, 0, 50, 20]
            self.num_hot_encoding[5] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,6]"), 0, 0, 0, 0, 50, 24]
            self.num_hot_encoding[6] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,7]"), 0, 0, 0, 0, 50, 28]
            self.num_hot_encoding[7] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,8]"), 0, 0, 0, 0, 50, 32]
            self.num_hot_encoding[8] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,9]"), 0, 0, 0, 0, 50, 36]
            self.num_hot_encoding[9] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,10]"), 0, 0, 0, 0, 50, 40]
            self.num_hot_encoding[10] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,11]"), 0, 0, 0, 0, 50, 44]
            self.num_hot_encoding[11] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,12]"), 0, 0, 0, 0, 50, 48]
            self.num_hot_encoding[12] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,13]"), 0, 0, 0, 0, 50, 52]
            self.num_hot_encoding[13] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,14]"), 0, 0, 0, 0, 50, 56]
            self.num_hot_encoding[14] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,15]"), 0, 0, 0, 0, 50, 60]
            self.num_hot_encoding[15] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,16]"), 0, 0, 0, 0, 50, 64]
            self.num_hot_encoding[16] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,17]"), 0, 0, 0, 0, 50, 68]
            self.num_hot_encoding[17] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,18]"), 0, 0, 0, 0, 50, 72]
            self.num_hot_encoding[18] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,19]"), 0, 0, 0, 0, 50, 76]
            self.num_hot_encoding[19] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,20]"), 0, 0, 0, 0, 50, 80]
            self.num_hot_encoding[20] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,21]"), 0, 0, 0, 0, 100, 84]
            self.num_hot_encoding[21] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,22]"), 0, 0, 0, 0, 100, 88]
            self.num_hot_encoding[22] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,23]"), 0, 0, 0, 0, 100, 92]
            self.num_hot_encoding[23] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,24]"), 0, 0, 0, 0, 100, 96]
            self.num_hot_encoding[24] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,25]"), 0, 0, 0, 0, 100, 100]

            self.num_hot_encoding[25] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,26]"), 0, 0, 0, 0, 50, 4]
            self.num_hot_encoding[26] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,27]"), 0, 0, 0, 0, 50, 8]
            self.num_hot_encoding[27] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,28]"), 0, 0, 0, 0, 50, 12]
            self.num_hot_encoding[28] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,29]"), 0, 0, 0, 0, 50, 16]
            self.num_hot_encoding[29] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,30]"), 0, 0, 0, 0, 50, 20]
            self.num_hot_encoding[30] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,31]"), 0, 0, 0, 0, 50, 24]
            self.num_hot_encoding[31] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,32]"), 0, 0, 0, 0, 50, 28]
            self.num_hot_encoding[32] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,33]"), 0, 0, 0, 0, 50, 32]
            self.num_hot_encoding[33] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,34]"), 0, 0, 0, 0, 50, 36]
            self.num_hot_encoding[34] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,35]"), 0, 0, 0, 0, 50, 40]
            self.num_hot_encoding[35] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,36]"), 0, 0, 0, 0, 50, 44]
            self.num_hot_encoding[36] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,37]"), 0, 0, 0, 0, 50, 48]
            self.num_hot_encoding[37] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,38]"), 0, 0, 0, 0, 50, 52]
            self.num_hot_encoding[38] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,39]"), 0, 0, 0, 0, 50, 56]
            self.num_hot_encoding[39] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,40]"), 0, 0, 0, 0, 50, 60]
            self.num_hot_encoding[40] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,41]"), 0, 0, 0, 0, 50, 64]
            self.num_hot_encoding[41] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,42]"), 0, 0, 0, 0, 50, 68]
            self.num_hot_encoding[42] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,43]"), 0, 0, 0, 0, 50, 72]
            self.num_hot_encoding[43] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,44]"), 0, 0, 0, 0, 50, 76]
            self.num_hot_encoding[44] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,45]"), 0, 0, 0, 0, 50, 80]
            self.num_hot_encoding[45] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,46]"), 0, 0, 0, 0, 50, 84]
            self.num_hot_encoding[46] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,47]"), 0, 0, 0, 0, 50, 88]
            self.num_hot_encoding[47] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,48]"), 0, 0, 0, 0, 50, 92]
            self.num_hot_encoding[48] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,49]"), 0, 0, 0, 0, 50, 96]
            self.num_hot_encoding[49] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,50]"), 0, 0, 0, 0, 50, 100]

            self.num_hot_encoding[50] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,51]"), 0, 0, 0, 0, 100, 4]
            self.num_hot_encoding[51] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,52]"), 0, 0, 0, 0, 100, 8]
            self.num_hot_encoding[52] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,53]"), 0, 0, 0, 0, 100, 12]
            self.num_hot_encoding[53] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,54]"), 0, 0, 0, 0, 100, 16]
            self.num_hot_encoding[54] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,55]"), 0, 0, 0, 0, 100, 20]
            self.num_hot_encoding[55] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,56]"), 0, 0, 0, 0, 100, 24]
            self.num_hot_encoding[56] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,57]"), 0, 0, 0, 0, 100, 28]
            self.num_hot_encoding[57] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,58]"), 0, 0, 0, 0, 100, 32]
            self.num_hot_encoding[58] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,59]"), 0, 0, 0, 0, 100, 36]
            self.num_hot_encoding[59] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,60]"), 0, 0, 0, 0, 100, 40]
            self.num_hot_encoding[60] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,61]"), 0, 0, 0, 0, 100, 44]
            self.num_hot_encoding[61] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,62]"), 0, 0, 0, 0, 100, 48]
            self.num_hot_encoding[62] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,63]"), 0, 0, 0, 0, 100, 52]
            self.num_hot_encoding[63] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,64]"), 0, 0, 0, 0, 100, 56]
            self.num_hot_encoding[64] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,65]"), 0, 0, 0, 0, 100, 60]
            self.num_hot_encoding[65] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,66]"), 0, 0, 0, 0, 100, 64]
            self.num_hot_encoding[66] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,67]"), 0, 0, 0, 0, 100, 68]
            self.num_hot_encoding[67] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,68]"), 0, 0, 0, 0, 100, 72]
            self.num_hot_encoding[68] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,69]"), 0, 0, 0, 0, 100, 76]
            self.num_hot_encoding[69] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,70]"), 0, 0, 0, 0, 100, 80]
            self.num_hot_encoding[70] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,71]"), 0, 0, 0, 0, 100, 84]
            self.num_hot_encoding[71] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,72]"), 0, 0, 0, 0, 100, 88]
            self.num_hot_encoding[72] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,73]"), 0, 0, 0, 0, 100, 92]
            self.num_hot_encoding[73] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,74]"), 0, 0, 0, 0, 100, 96]
            self.num_hot_encoding[74] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,75]"), 0, 0, 0, 0, 100, 100]

            self.num_hot_encoding[75] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,76]"), 0, 0, 0, 0, 100, 4]
            self.num_hot_encoding[76] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,77]"), 0, 0, 0, 0, 100, 8]
            self.num_hot_encoding[77] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,78]"), 0, 0, 0, 0, 100, 12]
            self.num_hot_encoding[78] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,79]"), 0, 0, 0, 0, 100, 16]
            self.num_hot_encoding[79] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,80]"), 0, 0, 0, 0, 100, 20]
            self.num_hot_encoding[80] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,81]"), 0, 0, 0, 0, 100, 24]
            self.num_hot_encoding[81] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,82]"), 0, 0, 0, 0, 100, 28]
            self.num_hot_encoding[82] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,83]"), 0, 0, 0, 0, 100, 32]
            self.num_hot_encoding[83] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,84]"), 0, 0, 0, 0, 100, 36]
            self.num_hot_encoding[84] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,85]"), 0, 0, 0, 0, 100, 40]
            self.num_hot_encoding[85] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,86]"), 0, 0, 0, 0, 100, 44]
            self.num_hot_encoding[86] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,87]"), 0, 0, 0, 0, 100, 48]
            self.num_hot_encoding[87] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,88]"), 0, 0, 0, 0, 100, 52]
            self.num_hot_encoding[88] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,89]"), 0, 0, 0, 0, 100, 56]
            self.num_hot_encoding[89] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,90]"), 0, 0, 0, 0, 100, 60]
            self.num_hot_encoding[90] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,91]"), 0, 0, 0, 0, 100, 64]
            self.num_hot_encoding[91] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,92]"), 0, 0, 0, 0, 100, 68]
            self.num_hot_encoding[92] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,93]"), 0, 0, 0, 0, 100, 72]
            self.num_hot_encoding[93] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,94]"), 0, 0, 0, 0, 100, 76]
            self.num_hot_encoding[94] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,95]"), 0, 0, 0, 0, 100, 80]
            self.num_hot_encoding[95] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,96]"), 0, 0, 0, 0, 100, 84]
            self.num_hot_encoding[96] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,97]"), 0, 0, 0, 0, 100, 88]
            self.num_hot_encoding[97] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,98]"), 0, 0, 0, 0, 100, 92]
            self.num_hot_encoding[98] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,99]"), 0, 0, 0, 0, 100, 96]
            self.num_hot_encoding[99] = [self.plantSim.getValue(".Modelle.Netzwerk.possible_jobs[1,100]"), 0, 0, 0, 0, 100, 100]

            self.observation = np.reshape(self.num_hot_encoding, (100, 7))

        #print(self.observation)
        else:
            self.reset()

        return self.observation

    def move(self,action):
        """
        Move the vehicle in the simulation given the action.
        :param action:
        :return:
        """
        self.plantSim.setValue(".Modelle.Netzwerk.job_row", int(action))
        self.count = self.plantSim.getValue(".Modelle.Netzwerk.count") #count = 1
        self.newCount = self.count + 1   # newCount = 2

        while self.count < self.newCount:   # 2 < 2
            self.count = self.plantSim.getValue(".Modelle.Netzwerk.count")
        self.newCount = self.count
        reward = self.plantSim.getValue(".Modelle.Netzwerk.reward")
        return reward


