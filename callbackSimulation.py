from os.path import exists
import csv
import numpy as np
import matplotlib.pyplot as plt
from rl.callbacks import Callback, TestLogger

class TestLogger(TestLogger):
    def on_episode_end(self, episode, logs):
        """ TestLogger2048 is a callback function that prints the logs at end of each episode/game match """

        template = 'episode: {episode}, episode reward: {episode_reward:.3f}, episode steps: {nb_steps}'
        variables = {
            'episode': episode + 1,
            'episode_reward': logs['episode_reward'],
            'nb_steps': logs['nb_steps']
        }
        #print(template.format(*variables))

class TrainEpisodeLogger(Callback):
    """
    TrainEpisodeLogger2048 is a callback function used to plot some charts, updated in real-time, that show the
    NN training results (max tile reached, average reward, etc.) and to save the results in a CSV file.
    """
    # Code modified from TrainEpisodeLogger

    def __init__(self, filePath):
        # Some algorithms compute multiple episodes at once since they are multi-threaded.
        # We therefore use a dictionary that is indexed by the episode to separate episodes
        # from each other.
        self.observations = {}
        self.rewards = {}
        self.step = 0

        self.episodes = []

        self.episodes_rewards = []
        self.fig_reward = plt.figure()
        self.ax2 = self.fig_reward.add_subplot(1,1,1) # 1 row, 1 column, 1st plot

        # Max Tiles Means/Episode Rewards Means: initialize variables and figures

        self.episodes_rewards_means = 0
        self.fig_reward_mean = plt.figure()
        self.ax4 = self.fig_reward_mean.add_subplot(1,1,1) # 1 row, 1 column, 1st plot'''

        self.nb_episodes_for_mean = 50 # calculate means after this amount of episodes
        self.episode_counter = 0 # Used to count the episodes and to decide when to calculate the average and plot it

        # CSV file:
        if exists(filePath):
            csv_file = open(filePath, "a") # a = append
            self.csv_writer = csv.writer(csv_file, delimiter=',')
        else:
            csv_file = open(filePath, "w") # w = write (clear and restart)
            self.csv_writer = csv.writer(csv_file, delimiter=',')
            headers = ['episode', 'episode_steps', 'episode_reward']
            self.csv_writer.writerow(headers)

    def on_episode_begin(self, episode, logs):
        """ Reset environment variables at beginning of each episode """
        self.observations[episode] = []
        self.rewards[episode] = []

    def on_episode_end(self, episode, logs):
        """ Compute and print training statistics of the episode when done but also plot training charts and save the data in a CSV file. """
        self.episode_counter += 1
        self.episodes = np.append(self.episodes, episode + 1)
        self.episodes_rewards = np.append(self.episodes_rewards, logs['episode_reward'])
        variables = {
            'step': self.step,
            #'nb_steps': self.params['nb_steps'],
            'episode': episode + 1,
            'episode_steps': len(self.observations[episode]),
            'episode_reward': self.episodes_rewards,
        }
        exp_episode = 'cos_one100,512-512, sm500000,500wu, 10000ep, av0-100, r 100'
        # Save CSV:
        self.csv_writer.writerow((episode + 1, len(self.observations[episode]), self.episodes_rewards[-1]))

        # Figures: Means
        # Graphs for values averaged across more episodes (e.g., averages over 50 episodes, etc.)
        if self.episode_counter % self.nb_episodes_for_mean == 0 :

            self.episodes_rewards_means = np.append(self.episodes_rewards_means,np.mean(self.episodes_rewards[-self.nb_episodes_for_mean:]))
            self.fig_reward_mean.clear()
            plt.figure(self.fig_reward_mean.number)
            plt.plot(np.arange(0,self.episode_counter+self.nb_episodes_for_mean,self.nb_episodes_for_mean), self.episodes_rewards_means)
            plt.title("rewards means (over last {} episodes)".format(self.nb_episodes_for_mean))
            plt.xlabel("episode #")
            plt.ylabel("rewards mean")
            plt.pause(0.01)
            plt.savefig('C:\\Users\\IFT\\Documents\\optimising-scheduling-problem-for-plant-simulation-system\\images\\%s_Episode_Reward_Mean.png' % exp_episode)    ## you need to change this value based on the number of episodes you are running(1000,500,..)

        # Figures: Points
        self.fig_reward.clear()
        plt.figure(self.fig_reward.number)
        plt.scatter(self.episodes, self.episodes_rewards, s=1) # scatterplot
        plt.plot(self.episodes, self.episodes_rewards) # line plot
        plt.title("reward (per episode)")
        plt.xlabel("episode #")
        plt.ylabel("reward")
        plt.pause(0.01)
        plt.savefig('C:\\Users\\IFT\\Documents\\optimising-scheduling-problem-for-plant-simulation-system\\images\\%s_Episode_Reward.png' % exp_episode)    ## you need to change this value based on the number of episodes you are running(1000,500,..)

        # Free up resources.
        del self.observations[episode]
        del self.rewards[episode]
        #del self.max_tile[episode]

    def on_step_end(self, step, logs):
        """ Update statistics of episode after each step """
        episode = logs['episode']
        self.observations[episode].append(logs['observation'])
        self.rewards[episode].append(logs['reward'])
        self.step += 1
